const dotenv = require('dotenv');

dotenv.config();

const port = process.env.SV_PORT || 9090

const restify = require('restify');


const corsMiddleware = require('restify-cors-middleware');

const cors = corsMiddleware({
    preflightMaxAge: 5, //Optional
    origins: ['*'],
    allowHeaders: ["Origin", "X-Requested-With", "Content-Type", "Accept", "Authorization"],
    exposeHeaders: ["Origin", "X-Requested-With", "Content-Type", "Accept", "Authorization"]
});


const errorHandler = require('restify-errors');

const jwt = require('jsonwebtoken');


var knex = require('knex')({
    client: 'sqlite3',
    connection: {
        filename: "./database.db"
    }
});


const server = restify.createServer({
    name: 'hackingDAay',
    version: '1.0.0'
});

//configure cors
server.pre(cors.preflight);
server.use(cors.actual);
server.use(restify.plugins.gzipResponse());

//configura o restify
server.use(restify.plugins.acceptParser(server.acceptable));
server.use(restify.plugins.queryParser());
server.use(restify.plugins.bodyParser());

//inicia o server
server.listen(port, function () {
    console.log('\033[2J'); //limpa o console
    console.log('Backend running at %s', server.url);
});

//configura gerenciador de rotas do servidor
require('./src/routes.js')({ server, errorHandler, knex, jwt });
