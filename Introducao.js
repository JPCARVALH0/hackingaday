

1 - Definições de variáveis

var x = 2;
const x = 0;
let x = 2;
x = 3;

2 - Operações

let x = 0;
let y = 0;

switch x {
    case: (x === 2){
            ...
    }
    default: {
            ...
    }
}



if (x === y) {

} else if () {

}
else {

}
if (x >= y) {

}
if (x <= y) {

}
x++;
x--;
x +=;
x -=;
x *=;

3 - Estruturas de dados

Javas Script Object Notation

let pessoa1 = {
    nome: "josé",
    idade: 30,
    telefone: '71988889900',
    historico: 
        }

pessoa1.idade = 30;

var frutas = [
    'bananna',
    'maçã',
    'uva',
    'pera'
]
const verduras = [
    'cenoura',
    'couve',
    'beterraba'
]


const cesta = [frutas, verduras];

[
    ['bananna', 'maçã', 'uva', 'pera'],
    ['cenoura', 'couve', 'beterraba']
]

const cesta = [...frutas, ...verduras];

[
    'bananna', 'maçã', 'uva', 'pera', 'cenoura', 'couve', 'beterraba'
]

let pessoa1 = {
    nome: "josé",
    idade: 30,
    telefone: '71988889900',
}

let pessoa2 = {
    apelido: "maria",
    idade: 32,
    telefone: 's/n',
}


let { apelido, idade } = pessoa2;

let pessoa3 = {
    ...pessoa1,
    ...pessoa2
}

pessoa3 = {
    nome: 'josé',
    idade: 32,
    telefone: 's/n',
    apelido: "maria"
}

pessoa3.nome;
pessoa3['nome'];


let obj = {
    'eletricista': 'jorge';
    'porteiro': 'jorge';
    'administrador': 'jorge';
    'trocador': 'maria';
}


obj['professor'] = 'pedro'

let obj = {
    'eletricista': 'jorge';
    'porteiro': 'jorge';
    'administrador': 'jorge';
    'trocador': 'pedro';
}


4 - laços de repetção

for (let index = 0; index < array.length; index++) {
    const element = array[index];

}

array.forEach(
    (element) => {

    }
);

for (const element of array) {

}

for (const key in object) {
    const element = object[key];

}

while (x < 1000) {
    x++;
}

5 - Funções

{
    let c = 0;
    function somaNumeros(a, b) {
        return a + b + c;
    }

}

const somaNumerosArrow = (a, b) => {
    return a + b;
}


let funcoes{
    funcao1: somaNumerosArrow
}

funcoes.somaNumerosArrow(2, 3);

6 - assincronia


async function somaNumeros(a, b) {
    return a + b;
}

let soma = await somaNumeros(3, 7);

somaNumeros(3, 7).then(soma => {
})

const tal = async () => {

}


async function printa(de, para) {
    for (let index = de; index <= para; index++) {
        console.log(index);
    }
}


printa(100000, 99999999999999999999999999999);
printa(1, 5);
printa(10, 50);

7 - HighOrder Functions

let nomes = [................];


let tem_nome = nomes.find(nome => nome === 'Ellen Mickaely');


let pessoas [{ nome: fulano, salario:50}....];


let salarios_acima_de_50 = pessoas.filter((element) => element.salario > 50);

function testaSalario(pessoa) {
    let x = 10;
    if (pessoa.salario > 50) {
        return true;
    } else {
        return false;
    }
}
function testaSalarioArrow(pessoa) {
    let salario_acima_50 = pessoa.salario.filter(salario => salario.mes === 'março' && salario.valor > 50);
    return salarios_acima_de_50;
}
const testaSalarioArrow = pessoa => (pessoa.salario > 50);

let salarios_acima_de_50 =
    pessoas.filter(
        pessoa => pessoa.salario.filter(
            salario => salario.mes === 'março' && salario.valor > 50)
    );



let salarios_acima_de_50 =
    pessoas.filter(testaSalarioArrow);





