const errorController = require('../controller/errorController.js');
const webCrawlerController = require('../controller/webCrawlerController.js');
const loginController = require('../controller/loginController');

module.exports = ({ server, errorHandler, knex, jwt }) => {

    server.get('/crawl/thingiverse/thing/:id', function (req, res) {
        try {
            webCrawlerController.crawlThingiverse(req, res, knex).catch(error => {
                errorController.returnInternalServerError(error, res);
            });
        } catch (error) {
            console.log(error);
            errorController.returnInternalServerError(error, res);
        }
    })
}