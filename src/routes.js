const loginRoutes = require("./routes/loginRoutes.js");
const userRoutes = require("./routes/userRoutes.js");
const webCrawlerRoutes = require("./routes/webCrawlerRoutes.js");

module.exports = (config_params) => {
    loginRoutes(config_params);
    userRoutes(config_params);
    webCrawlerRoutes(config_params);
}