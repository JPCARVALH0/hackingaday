
const sha1 = require('sha1');
const dotenv = require('dotenv');
dotenv.config()

module.exports = {
    async loginDoUsuario(req, res, knex, jwt) {
        let { email, senha } = req.body;

        let user = await knex('user').where('email', email).andWhere('senha', sha1(senha)).first();

        if (user) {
            //autenticou
            delete (user.senha);

            jwt.sign({ user }, process.env.SV_SECRET, async (err, token) => {
                if (!err) {
                    res.send({ status: "success", data: { user, token } });
                } else {
                    throw new Error('Token não pode ser assinado digitalmente.');
                }

            });

        } else {
            //não autenticou
            throw Error('Usuário não encontrado')
        }

    },

    verifyToken(req, jwt) {
        let bearerHeader = req.headers['authorization'];
        if (bearerHeader) {
            const tkn = bearerHeader.split(' ')[1];
            let user = jwt.verify(tkn, process.env.SV_SECRET, (err, tokenData) => {
                if (err) {
                    return false;
                } else {
                    return tokenData.user;
                }
            });
            return user;
        } else {
            return false;
        }
    }
}
