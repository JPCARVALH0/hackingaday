const ppt = require('puppeteer');
const { installMouseHelper } = require('../helpers/install-mouse-helper');

module.exports = {
    async crawlThingiverse(req, res, knex) {
        let { id } = req.params;

        const baseURL = 'https://www.thingiverse.com/thing:';

        //launch browser so we can see what is going on
        const browser = await ppt.launch({ headless: false, defaultViewport: { width: 1920, height: 1080 }, args: ['--start-maximized'], isLandscape: true });
        //const browser = await ppt.launch();
        const page = await browser.newPage();
        await installMouseHelper(page);
        // let url = baseURL + id;
        let url = 'https://www.google.com.br';

        //aguarda o carregamento da URL
        await Promise.all([
            page.goto(url),
            page.waitForNavigation({ waitUntil: 'networkidle2' })
        ]);

        console.log('navegou');

        // const logMemory = () => {
        //     const used = process.memoryUsage();
        //     // console.log(''.padStart(5, '\n'));
        //     let memo = [];
        //     for (let key in used) {
        //         memo.push({ key, size: `${Math.round(used[key] / 1024 / 1024 * 100) / 100} MB`.padStart(8, ' ') });
        //         // console.log(`${key} ${Math.round(used[key] / 1024 / 1024 * 100) / 100} MB`);
        //     }
        //     console.table(memo);
        //     console.log(''.padStart(50, '-'));
        // }

        // setInterval(() => {
        //     logMemory();

        // }, 1000);


        let info = await page.evaluate((page) => {
            let inputs = document.querySelectorAll('input');
            for (let input of inputs) {
                if (input.getAttribute('title') === 'Pesquisar') {
                    input.value = 'teste de busca';
                }
            }

        });
        page.mouse.move(850, 470);
        page.mouse.click(850, 470);
        // page.mouse.click(1400, 550);


        // page.keyboard.down('MetaLeft');
        // page.keyboard.press('ArrowUp');
        // page.keyboard.up('MetaLeft');

        // let info = await page.evaluate(() => {
        //     //aqui ocorre a mágica. Imagine que vc está dentro do site. basta inserir o código aqui.

        //     console.log('rodando nossa função no brwsr');

        //     let meu_li = document.querySelector('.slide.selected');
        //     let img = meu_li.children[0];
        //     let link = img.src;

        //     console.log(link);

        //     return link;


        // });

        // browser.close();
        res.json({ status: "success", data: { info } });


    },

}
